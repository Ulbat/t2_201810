package model.logic;

import java.io.FileReader;

import com.google.gson.Gson;
import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.ListaDoble;

public class TaxiTripsManager implements ITaxiTripsManager {

	ListaDoble<Service> servicios = new ListaDoble<Service>();
	ListaDoble<Taxi> taxis = new ListaDoble<Taxi>();
	// TODO
	// Definition of data model 

	public void loadServices (String serviceFile)

	{
		Gson jaison = new Gson() ;
		try {
			FileReader fr = new FileReader(serviceFile);
			Service [] servi = jaison.fromJson(fr, Service[].class);
			for (int i = 0; i < servi.length; i++) 
			{
				if (servi[i].getCompany()!=null) 
				{
					String viejito = servi[i].getCompany();
					String nuevo = viejito.replaceAll("\\s", "");
					servi[i].setCompany(nuevo);
				}
				servicios.addEnd(servi[i]);
			}
			for (Service servicio : servi) 
			{
				for (Taxi te: taxis) {
					if (te.getTaxi_id().equalsIgnoreCase(servicio.getTaxiId())) 
					{
						Taxi tax = new Taxi();
						if (tax.getCompany().equals(null))
						{
							tax.setCompany("freedom");
						}
						else
						{
							tax.setCompany(servicio.getCompany());
						}
						tax.setTaxi_id(servicio.getTaxiId());
						taxis.addEnd(tax);
					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		ListaDoble<Taxi> listaTaxi = new ListaDoble<Taxi>();
		for (Taxi taxi : taxis) 
		{
			if (taxi.getCompany().equalsIgnoreCase(company)) 
			{
				listaTaxi.addFirst(taxi);
			}
		}
		System.out.println("Inside getTaxisOfCompany with " + company);
		return listaTaxi;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		ListaDoble<Service> listaservi = new ListaDoble<Service>();
		for (Service servid : servicios) 
		{
			if (servid.getPickupCommunityArea().equalsIgnoreCase("communityArea") );
			{
				listaservi.addFirst(servid);
			}
		}		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return listaservi;
	}


}
