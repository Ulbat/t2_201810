package model.data_structures;

import java.util.Iterator;

public class ListaDoble<T>implements LinkedList<T> {
	private Nodo<T> lista;

	private int tamLista;
	

	public ListaDoble()
	{
		lista = null;
	}
	public ListaDoble (T item)
	{
		lista = new Nodo<T> (item);
		tamLista++;
	}
	
	public T darElementoActual()
	{
		return lista.darItem();
	}
	

	public void addFirst ( T item)
	{
		Nodo<T> nuevaCabeza = new Nodo<T>(item);
		if (lista ==null)
		{
			lista = nuevaCabeza;
			tamLista++;
		}
		else 
		{

			nuevaCabeza.cambiarSiguiente(lista);
			lista.cambiarAnterior(nuevaCabeza);
			lista = nuevaCabeza;

			tamLista++;
		}


	}
	public void addEnd (T item)
	{
		Nodo<T> nuevo = new Nodo<T>(item);
		if (lista == null) 
		{
			lista = nuevo;
		}
		else
		{
			Nodo<T> actual = lista;
			while (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			actual.cambiarSiguiente(nuevo);
		}
		tamLista++;
	}
	public void eliminar(T item)
	{
		if (lista != null) 
		{
			Nodo<T> actual = lista;
			if (actual.darItem().equals(item)) 
			{
				Nodo <T> nuevoActual = actual.darSiguiente();
				nuevoActual.darAnterior().cambiarSiguiente(null);
				lista.cambiarSiguiente(nuevoActual);
				actual.cambiarSiguiente(null);
				tamLista--;
			}
			else
			{
				boolean boo = true;
				while (actual.darSiguiente()!=null&&boo) 
				{
					if(actual.darItem().equals(item))
					{
						Nodo <T> nuevoActual = actual.darSiguiente();
						nuevoActual.darAnterior().cambiarAnterior(actual.darAnterior());
						actual.darAnterior().cambiarSiguiente(nuevoActual);
						tamLista--;
						boo= false;
					}
				}
			}
		}
	}
	public int darTam()
	{
		return tamLista;
	}
	public T darElementoEnLaPosicion(int i)
	{
		if (lista==null) {
			return null;
		}
		else
		{
			Nodo<T> jesus = lista;
			int cont=0;
			while (jesus.darSiguiente()!=null&&i!=cont) 
			{
				jesus = jesus.darSiguiente();
				cont++;
			}
			return jesus.darItem();
		}

	}
	@Override
	public Iterator<T> iterator() 
	{
		return new listIterator();
	}
	private class listIterator implements Iterator<T> {
		Nodo<T> jesus = lista;

		@Override
		public boolean hasNext() 
		{
			if (tamLista==0){
				return false;}
			if (jesus!=null) 
				return true;
			return jesus.darSiguiente()!=null;
		}

		@Override
		public T next() {
			if (jesus==null) 
			{
				jesus=lista;
			}		
			else
			{
				jesus= jesus.darSiguiente();
			}
			return jesus.darItem();
		}
	}

}


