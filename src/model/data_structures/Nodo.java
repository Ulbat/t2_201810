package model.data_structures;

public class Nodo <E> {

	private Nodo<E> siguiente;
	private E item;
	private Nodo <E> anterior;

	public Nodo (E item)
	{
		siguiente = null;	
		this.item = item;
	}
	
	public Nodo <E> darSiguiente()
	{return siguiente;}
	public Nodo <E> darAnterior()
	{return anterior;}
	public void cambiarSiguiente (Nodo<E> sig)
	{this.siguiente =  sig;}
	public void cambiarAnterior (Nodo<E> sig)
	{this.anterior =  sig;}
	public E darItem ()
	{return item;}
	public void cambiarItem (E it)
	{this.item = it;}
}
