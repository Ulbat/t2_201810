package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> 
{
	private String trip_id;
	private String taxi_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private String company;
	private String pickup_community_area;

	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId()
	{
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles()
	{
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return trip_total;
	}
	public String  getCompany() 
	{
		return company;
	}
	@Override
	public int compareTo(Service o) 
	{
		return 0;
	}
	
	
	public void setCompany(String company)
	{
		this.company = company;
	}
	public String getPickupCommunityArea()
	{
		return pickup_community_area;
	}
	public void setPickupCommunityArea(String p)
	{
		this.pickup_community_area = p;
	}
}

